export interface Message {
    text?: string,
    timeStamp?: number
}