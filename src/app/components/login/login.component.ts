import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public authService: AuthService) { }

  login(): void{
    this.authService.signIn();
  }

  logout(): void{
    this.authService.signOut();
  }

  ngOnInit(): void {
  }

}
