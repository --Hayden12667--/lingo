// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA4BvGyLw8P4TIB03QeA73Ozl7ba1xQEeE",
    authDomain: "lingo-database.firebaseapp.com",
    projectId: "lingo-database",
    storageBucket: "lingo-database.appspot.com",
    messagingSenderId: "497269633517",
    appId: "1:497269633517:web:4cff9d364c372a0048f7e4",
    measurementId: "G-NXPZ1KGXG6"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
